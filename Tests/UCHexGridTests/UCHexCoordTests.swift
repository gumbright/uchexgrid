//
//  UCHexCoordTests.swift
//  
//
//  Created by Guy Umbright on 3/5/22.
//

import XCTest
import UCHexGrid

class UCHexCoordTests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testAddition() throws {
        let c1 = UCHexCoord(q: 10, r: 10)
        let c2 = UCHexCoord(q: 2, r: -3)
        let c3 = c1 + c2
        XCTAssertTrue(c3.q == 12)
        XCTAssertTrue(c3.r == 7)
    }

    func testSubtraction() throws {
        let c1 = UCHexCoord(q: 10, r: 10)
        let c2 = UCHexCoord(q: 12, r: 3)
        let c3 = c1 - c2
        XCTAssertTrue(c3.q == -2)
        XCTAssertTrue(c3.r == 7)
    }

//    func testPerformanceExample() throws {
//        // This is an example of a performance test case.
//        self.measure {
//            // Put the code you want to measure the time of here.
//        }
//    }

}
