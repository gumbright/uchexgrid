import XCTest
@testable import UCHexGrid

final class UCHexGridTests: XCTestCase {
    func testCache() throws {
        
        let config = UCHexGridConfiguration(hexType: .flat, radius: 20.0)
        let grid = UCHexGrid(config: config, cache: true)
        
        let _ = grid.hex(at: UCHexCoord(q: 10, r: 10))
        let hex2 = grid.hex(at: UCHexCoord(q: 20, r: 10))
        let _ = grid.hex(at: UCHexCoord(q: 20, r: 20))
        let hex4 = grid.hex(at: UCHexCoord(q: 20, r: 10))
        
        XCTAssertTrue(grid.cacheSize == 3)
        XCTAssertTrue(hex2 === hex4)
        
        grid.clearCache()
        
        XCTAssertTrue(grid.cacheSize == 0)
    }
    
    func testPointToHex() throws
    {
        let config = UCHexGridConfiguration(hexType: .point, radius: 20.0, border: 20.0)
        let grid = UCHexGrid(config: config, cache: false)
        
        let h1 = grid.hex(for: UCPoint(x: 0, y: 0))
        let h2 = grid.hex(for: UCPoint(x: 50, y: 105))
        let h3 = grid.hex(for: UCPoint(x: 75, y: -133))
        let h4 = grid.hex(for: UCPoint(x: -125, y: -60))
        let h5 = grid.hex(for: UCPoint(x: -75, y: 60))

        XCTAssertTrue(h1.q == 0 && h1.r == 0)
        XCTAssertTrue(h2.q == 0 && h2.r == 2)
        XCTAssertTrue(h3.q == 3 && h3.r == -3)
        XCTAssertTrue(h4.q == -2 && h4.r == -1)
        XCTAssertTrue(h5.q == -2 && h5.r == 1)
    }
    
    func testHitTest() throws
    {
        let config = UCHexGridConfiguration(hexType: .point, radius: 20.0, border: 20.0)
        let grid = UCHexGrid(config: config, cache: false)
        
        let h = grid.hexContentHit(for: UCPoint(x: 0, y: 30))
        XCTAssert( h == nil)
        
        let h2 = grid.hexContentHit(for: UCPoint(x: 0, y: 15))
        XCTAssert( h2!.q == 0 && h2!.r == 0)
    }
}
