import XCTest
@testable import UCHexGrid

final class UCHexTests: XCTestCase {
//    func testSetup() throws
//    {
//        let config = UCHexGridConfiguration(hexType: .flat, radius: 10.0);
//        let coord = UCHexCoord(q:0, r:0)
//        let hex = UCHex(coord: coord, config: config)
//
//        XCTAssertNotNil(hex.center)
//        XCTAssertTrue(hex.verticies.count == 6)
//    }
    
    func testPosition() throws
    {
        let config = UCHexGridConfiguration(hexType: .point, radius: 10.0, border: 10.0)
        let grid = UCHexGrid(config: config, cache: true)
        
        var hexes : [UCHex] = []
        hexes.append(grid.hex(at: UCHexCoord(q: 6, r: 6)))
        hexes.append(grid.hex(at: UCHexCoord(q: 7, r: 6)))
        hexes.append(grid.hex(at: UCHexCoord(q: 7, r: 7)))
        hexes.append(grid.hex(at: UCHexCoord(q: 8, r: 7)))
        hexes.append(grid.hex(at: UCHexCoord(q: 8, r: 8)))
        
        for hex in hexes
        {
            print("\(hex.coord.q),\(hex.coord.r) - \(hex.center.x),\(hex.center.y)")
        }
    }
    
    func testNeighbors() throws
    {
        let config = UCHexGridConfiguration(hexType: .point, radius: 10.0, border: 10.0)
        let grid = UCHexGrid(config: config, cache: true)

        let h = grid.hex(at: UCHexCoord(q: 3, r: -7))
        let neighbors = h.neighbors()
        
        var nCoordsOffsets = Set<UCHexCoord>()
        for n in neighbors
        {
            nCoordsOffsets.insert( n.coord - h.coord)
        }
        
        for nc in neighborCoords
        {
            XCTAssertTrue(nCoordsOffsets.contains(nc))
        }
    }
}
