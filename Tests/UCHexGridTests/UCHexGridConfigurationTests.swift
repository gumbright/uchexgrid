//
//  File.swift
//  
//
//  Created by Guy Umbright on 2/4/22.
//

import XCTest
@testable import UCHexGrid

final class UCHexGridConfigurationTests: XCTestCase
{
        func testSetup() throws
        {
            let config = UCHexGridConfiguration(hexType: .flat, radius: 10.0);
            
            XCTAssertTrue(config.radians.count == 6)
        }

}
