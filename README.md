# UCHexGrid

UCHexGrid is an abstracted (mostly) Swift library for generating a hex based grid for MacOS & iOS. It is based on the REALLY EXCELLENT article from Red Blob Games https://www.redblobgames.com/grids/hexagons/

UXHexGrid uses axial coordinates (please see the above link for more info...seriously if you are dealing with a hex grid you should read it). UCHexGrid allows for arbitrary hex radius and border size and can generate a grid for either pointy for flat topped hexes.

With regard to implementation the short version is you create a UCHexGridConfiguration object and set the details of the desired hex grid. You then create a UCHexGrid object (passing it the configuration object). Then you use UCHexGrid to vend UCHex object which contain information about a particular hex specified via an axial coordinate.  UCHexGrid also provides getting what hex corresponds to x,y coordinates and hit testing.

Dependencies
- Measurement
- Core Graphics

# LICENSE
The MIT License (MIT)
Copyright © 2022 Guy Umbright

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
