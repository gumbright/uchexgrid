//
//  File.swift
//  
//
//  Created by Guy Umbright on 2/2/22.
//

import Foundation

/// Provides specifications creation of a hex grid
public class UCHexGridConfiguration : Codable
{
    /// Defines the style of hex for the grid
    public enum HexType : Codable
    {
        case flat,point
    }
    
    internal let flatAngles = [Measurement(value: 0, unit: UnitAngle.degrees),
                      Measurement(value: 60, unit: UnitAngle.degrees),
                      Measurement(value: 120, unit: UnitAngle.degrees),
                      Measurement(value: 180, unit: UnitAngle.degrees),
                      Measurement(value: 240, unit: UnitAngle.degrees),
                      Measurement(value: 300, unit: UnitAngle.degrees)]
    internal let pointAngles = [Measurement(value: 30, unit: UnitAngle.degrees),
                       Measurement(value: 90, unit: UnitAngle.degrees),
                       Measurement(value: 150, unit: UnitAngle.degrees),
                       Measurement(value: 210, unit: UnitAngle.degrees),
                       Measurement(value: 270, unit: UnitAngle.degrees),
                       Measurement(value: 330, unit: UnitAngle.degrees)]

    internal let flatRadians : [Measurement<UnitAngle>]
    internal let pointRadians : [Measurement<UnitAngle>]

    /// Hex type specified at initialization
    public private(set) var hexType : HexType = .flat

    /// Hex border width specified at initialization
    public private(set) var borderWidth = 1.0

    /// Radius specified at initialization
    var radius : Double = 10.0

    /// Calculated radius of the hex content area (radius - border width)
    public let contentRadius : Double

    /// Calculated radius of hex border center line (radius + (border width * 0.5))
    public let borderCenterRadius : Double

    /// Calculated radius of outer edge of hex (radius + border width)
    public let outerRadius : Double

    /// Origin of hex grid (position of (0,0) hext)
    public var origin = UCPoint(x: 0, y: 0)
 
    /// Default initializer
    public init (hexType : HexType, radius : Double, border : Double)
    {
        self.hexType = hexType
        self.radius = radius
        borderWidth = border
        contentRadius = radius
        borderCenterRadius = radius + (borderWidth / 2.0)
        outerRadius = radius+borderWidth

        //!!!this should be class level so not doing it for each hex
        flatRadians = flatAngles.map {$0.converted(to: .radians)}
        pointRadians = pointAngles.map {$0.converted(to: .radians)}
    }
    
    /// Conveniece initializer that assume a border size of 1
    public convenience init(hexType : HexType, radius : Double)
    {
        self.init(hexType: hexType, radius: radius, border: 1.0)
    }
    
    internal var radians : [Measurement<UnitAngle>]
    {
        get {hexType == .flat ? flatRadians : pointRadians}
    }
    
}
