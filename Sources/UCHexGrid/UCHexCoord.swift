//
//  File.swift
//  
//
//  Created by Guy Umbright on 2/2/22.
//

import Foundation

//using axial coordinates
//for flat , q is vertical
//for point, r is horizontal

/// Axial coordinates for a hex in the grid
///
/// For flat top hexes, q is the vertical axis
/// For point top hexes, r is the horizontal axis
public struct UCHexCoord : Hashable, Codable
{
    public var q : Int = 0
    public var r : Int = 0
    
    //this is stupid
    public init(q:Int, r:Int)
    {
        self.q = q
        self.r = r
    }
    
    ///Generally used to generate offset from a coordinate
    public static func +(left: UCHexCoord, right: UCHexCoord) -> UCHexCoord
    {
        return UCHexCoord(q: left.q + right.q, r: left.r + right.r)
    }

    ///Generally used to generate a delta between 2 coordinates
    public static func -(left: UCHexCoord, right: UCHexCoord) -> UCHexCoord
    {
        return UCHexCoord(q: left.q - right.q, r: left.r - right.r)
    }

    internal var cube : UCHexCoordCube
    {
        get {
            return UCHexCoordCube(q:q, r:r, s:-q-r)
        }
    }
}

///Internal use
internal struct UCHexCoordCube : Hashable
{
    var q : Int = 0
    var r : Int = 0
    var s : Int = 0
    //this is stupid
    init(q:Int, r:Int, s:Int)
    {
        self.q = q
        self.r = r
        self.s = s
    }
    
    var axial : UCHexCoord
    {
        get {
            return UCHexCoord(q: q, r: r)
        }
    }
}

///Internal use
internal struct UCHexCoordFrac
{
    public var q : Double = 0.0
    public var r : Double = 0.0
    
    public init(q: Double, r: Double)
    {
        self.q = q
        self.r = r
    }
    
    var cube : UCHexCoordCubeFrac
    {
        get {
            return UCHexCoordCubeFrac(q:q, r:r, s:-q-r)
        }
    }
}

///Internal use
internal struct UCHexCoordCubeFrac
{
    public var q : Double = 0.0
    public var r : Double = 0.0
    public var s : Double = 0.0

    public init(q: Double, r: Double, s: Double)
    {
        self.q = q
        self.r = r
        self.s = s
    }
}
