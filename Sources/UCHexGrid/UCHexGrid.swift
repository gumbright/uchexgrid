//desired function
//define hex size via radius, specify border width
//given coords, return hex definition
//set pointy top vs flat top

//Q on what type of coord system to user (or provide mult)
// https://www.redblobgames.com/grids/hexagons/

import CoreGraphics
/// Cartesian coordinate point structure
public struct UCPoint : Codable
{
    var x : Double
    var y : Double
    
    public init(x: Double, y:Double)
    {
        self.x = x
        self.y = y
    }
}

/// Represents the whole of the hex grid
public class UCHexGrid
{
    /// Configuration object specified at initialization
    public let config : UCHexGridConfiguration
    
    /// Whether caching is active, specified at initialization
    public let useCaching : Bool
    
    /// Reference hex with coord of 0,0
    ///
    /// (not cached but if caching is on any you request the 0,0 hex that one will be)
    public lazy var identityHex : UCHex! = UCHex(coord: UCHexCoord(q: 0, r: 0), grid: self)

    /// UCHex object cache
    private(set) var cache : [UCHexCoord:UCHex] = [:]
    
    /// Current size of the hex cache
    var cacheSize : Int
    {
        get {
            return cache.count
        }
    }
    
    /// Default initializer
    public init(config : UCHexGridConfiguration, cache: Bool = false)
    {
        self.config = config
        self.useCaching = cache
    }
    
    /// Return UCHex for specifed coordinate
    public func hex(at coord : UCHexCoord) -> UCHex
    {
        if let hex = cache[coord]
        {
            return hex
        }
        else
        {
            let hex = UCHex(coord: coord, grid: self)
            if useCaching
            {
                cache[coord] = hex
            }
            return hex
        }
    }
    
    /// Clear the hex cache
    public func clearCache()
    {
        cache = [:]
    }
}

extension UCHexGrid //hit test
{
    /// Returns coordinates of hex that contains the a point
    public func hex(for point : UCPoint) -> UCHexCoord
    {
        return config.hexType == .flat ? flatHex(for: point) : pointHex(for: point)
    }
    
    /// Returns coordinates of hex with content radius that contains the point.  Returns nil if none.
    public func hexContentHit(for point : UCPoint) -> UCHexCoord?
    {
        //first get the hex
        let hexCoord = hex(for: point)
        
        //now we have the hex, need to see if the hit was is the content, not the border
        let path = hex(at: hexCoord).contentPath()
        let hit = path.contains(point.cgpoint)
        return hit ? hexCoord : nil
    }
    
}

extension UCHexGrid //private funcs
{
    internal func flatHex(for point : UCPoint) -> UCHexCoord
    {
        //as border center is the line hexes are packed
        
        let q = ( 2.0/3 * point.x                        ) / config.borderCenterRadius
        let r = (-1.0/3 * point.x  +  sqrt(3)/3 * point.y) / config.borderCenterRadius
        return axialRound(coord: UCHexCoordFrac(q: q, r: r))
        
    }
    
    internal func pointHex(for point : UCPoint) -> UCHexCoord
    {
        //!!!will need to handle border issues
        let q = (sqrt(3)/3 * point.x  -  1.0/3 * point.y) / config.borderCenterRadius
        let r = (                        2.0/3 * point.y) / config.borderCenterRadius
        return axialRound(coord: UCHexCoordFrac(q: q, r: r))
        
    }
    
    internal func axialRound(coord: UCHexCoordFrac) -> UCHexCoord
    {
        ///???this needs to account for border? With a wide border point could be in border between 2 hexes.  and if add spacing, could be in space
        //return cube_to_axial(cube_round(axial_to_cube(hex)))
        return cubeRound(coord: coord.cube).axial
    }
    
    internal func cubeRound(coord: UCHexCoordCubeFrac) -> UCHexCoordCube
    {
        var q = round(coord.q)
        var r = round(coord.r)
        var s = round(coord.s)
        
        let q_diff = abs(q - coord.q)
        let r_diff = abs(r - coord.r)
        let s_diff = abs(s - coord.s)
        
        if q_diff > r_diff && q_diff > s_diff
        {
            q = -r-s
        }
        else if r_diff > s_diff
        {
            r = -q-s
        }
        else
        {
            s = -q-r
        }
        
        return UCHexCoordCube(q: Int(q), r: Int(r), s: Int(s))
    }

}

