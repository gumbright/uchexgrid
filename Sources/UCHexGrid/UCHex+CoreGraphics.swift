//
//  File.swift
//  
//
//  Created by Guy Umbright on 2/5/22.
//
import CoreGraphics

public extension UCHex
{
    func contentPath() -> CGPath
    {
        return generatePath(verticies: contentVerticies)
    }
    
    func borderCenterPath() -> CGPath
    {
        return generatePath(verticies: borderCenterVerticies)
    }
    
    func outerPath() -> CGPath
    {
        return generatePath(verticies: outerVerticies)
    }
    
    private func generatePath(verticies : [UCPoint]) -> CGPath
    {
        let path = CGMutablePath()
        path.move(to: verticies[0].cgpoint)
        for i in 1...5
        {
            path.addLine(to: verticies[i].cgpoint)
        }
        path.addLine(to: verticies[0].cgpoint)
        path.closeSubpath()
        return path
    }

}

public extension UCPoint
{
    var cgpoint : CGPoint
    {
        get {
            return CGPoint(x: x, y: y)
        }
    }
}

public extension CGPoint
{
    var ucpoint : UCPoint
    {
        get {
            return UCPoint(x: x, y:y)
        }
    }
}
