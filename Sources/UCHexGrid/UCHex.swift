//
//  File.swift
//  
//
//  Created by Guy Umbright on 2/2/22.
//

import Foundation

/// Representation of a hex in the hex grid

let neighborCoords = [UCHexCoord(q: 0, r: -1), UCHexCoord(q: -1, r: 0), UCHexCoord(q: -1, r: 1),
                      UCHexCoord(q: 0, r: 1), UCHexCoord(q: 1, r: 0), UCHexCoord(q: 1, r: -1)]

public class UCHex
{
    public let coord : UCHexCoord
    public private(set) weak var grid : UCHexGrid!
    
    public var contentVerticies : [UCPoint]!
    {
        get {
            if let v = _contentVerticies
            {
                return v
            }
            else
            {
                _contentVerticies = calculateVerticies(center: center, radius: grid.config.contentRadius)
                return _contentVerticies
            }
        }
    }
    private         var _contentVerticies : [UCPoint]?
    
    public var borderCenterVerticies : [UCPoint]!
    {
        get {
            if let v = _borderCenterVerticies
            {
                return v
            }
            else
            {
                _borderCenterVerticies = calculateVerticies(center: center, radius: grid.config.borderCenterRadius)
                return _borderCenterVerticies
            }
        }
    }
    private         var _borderCenterVerticies : [UCPoint]?
    
    public var outerVerticies : [UCPoint]!
    {
        get {
            if let v = _outerVerticies
            {
                return v
            }
            else
            {
                _outerVerticies = calculateVerticies(center: center, radius: grid.config.outerRadius)
                return _outerVerticies
            }
        }
    }
    private         var _outerVerticies : [UCPoint]?
    
    public private(set) var center : UCPoint!

    public init(coord : UCHexCoord, grid : UCHexGrid)
    {
        self.coord = coord
        self.grid = grid
        center = position()
    }
    
}

extension UCHex //private funcs
{

    private func calculateVerticies(center : UCPoint, radius: Double) -> [UCPoint]
    {
        var v : [UCPoint] = []
        for i in 0...5
        {
            v.append(hex_corner(center: center, radius: radius, ndx: i))
            print(v)
        }
        
        return v
    }
    
    private func hex_corner(center : UCPoint, radius: Double, ndx : Int) -> UCPoint
    {
        return grid.config.hexType == .flat ? flat_hex_corner(center: center, radius: radius, ndx: ndx) : pointy_hex_corner(center: center, radius: radius, ndx: ndx)
    }
    
    //should have encapsulating method for these pairs and have the ternary handled there, keeps higher level cleaner
    private func pointy_hex_corner(center : UCPoint, radius: Double, ndx : Int) -> UCPoint
    {
        let angle_rad = grid.config.pointRadians[ndx].value
        return UCPoint(x: center.x + radius * cos(angle_rad),
                       y: center.y + radius * sin(angle_rad))
    }
    
    private func flat_hex_corner(center : UCPoint, radius: Double, ndx : Int) -> UCPoint
    {
        let angle_rad = grid.config.flatRadians[ndx].value
        return UCPoint(x: center.x + radius * cos(angle_rad),
                       y: center.y + radius * sin(angle_rad))
    }
    
    private func position() -> UCPoint
    {
        let rawPosition = self.grid.config.hexType == .flat ? flatPosition() : pointyPosition()
        return UCPoint(x: rawPosition.x + grid.config.origin.x, y: rawPosition.y + grid.config.origin.y )
    }
    
    private func flatPosition() -> UCPoint
    {
        let x = grid.config.borderCenterRadius * (     3.0 / 2 * Double(coord.q))
        let a = sqrt(3)/2 * Double(coord.q)
        let b = sqrt(3) * Double(coord.r)
        let y = grid.config.borderCenterRadius * (a  +  b)
        return UCPoint(x: x, y: y)
    }
    
    private func pointyPosition() -> UCPoint
    {
        let a = sqrt(3) * Double(coord.q)
        let b = sqrt(3)/2 * Double(coord.r)
        let x = grid.config.borderCenterRadius * (a  +  b)
        let y = grid.config.borderCenterRadius * (                         3.0/2 * Double(coord.r))
        return UCPoint(x: x, y: y)
    }

}

extension UCHex
{
    public func neighbors() -> [UCHex]
    {
        var result : [UCHex] = []
        for nDelta in neighborCoords
        {
            let nCoord = coord + nDelta
            result.append(grid.hex(at: nCoord))
        }
        return result
    }
}

extension UCHex : Equatable
{
    public static func == (lhs: UCHex, rhs: UCHex) -> Bool {
        return lhs.coord == rhs.coord
    }
}

extension UCHex : Hashable
{
    public func hash(into hasher: inout Hasher) {
        hasher.combine(coord.q)
        hasher.combine(coord.r)
    }
}
