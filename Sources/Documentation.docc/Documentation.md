# ````

Abstacted-ish framework for creating a hex grid

## Overview

Before anything else I need to call out the basis for this framework.  This excellent article is a master class in dealing with hex grids: https://www.redblobgames.com/grids/hexagons/


## Topics

### <!--@START_MENU_TOKEN@-->Group<!--@END_MENU_TOKEN@-->

- <!--@START_MENU_TOKEN@-->``Symbol``<!--@END_MENU_TOKEN@-->
